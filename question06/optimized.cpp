#include <iostream>
#include <cmath>

using namespace std;

int main() {
    int N = 50;
    int cnt = 0;

    for (int a = 1; a * a <= N; a++) {
        int b = sqrt(N);
        while (a * a + b * b > N)
            b--;
        
        if (a * a + b * b == N) {
            cnt++;
            cout << a << " " << b << endl;
        }
    }

    cout << "Total number of pairs (a, b): " << cnt << endl;

    return 0;
}

