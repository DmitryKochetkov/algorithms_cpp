#include <iostream>
#include <cmath>

using namespace std;

int main() {
    int N = 50;
    int cnt = 0;

    for (int a = 1; a <= N; a++)
        for (int b = 1; b <= N; b++)
            if (a * a + b * b == N) {
                cout << a << " " << b << endl;
                cnt += 1;
            }

    cout << "Total number of pairs (a, b): " << cnt << endl;

    return 0;
}

