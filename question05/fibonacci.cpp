#include <iostream>

long fib(long n) {
    if ((n == 0) || (n == 1))
        return 1;

    long a = 1;
    long b = 1;
    long result = 1;

    for (int i = 2; i < n; i++) {
        result = a + b;
        a = b;
        b = result;
    }

    return result;
}

using namespace std;

int main() {
    int n = 10;
    for (int i = 1; i < n+1; i++)
        cout << fib(i) << endl;
    return 0;
}