// #define _GLIBCXX_DEBUG

#include <iostream>
#include <vector>

using namespace std;

int main() {
    std::cout << "Question 8, UB example" << std::endl;
    std::vector <int> a(100);
    int x = -1;
    cout << a[x] << endl;
}