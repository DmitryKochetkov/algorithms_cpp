#include <iostream>
#include <vector>

using namespace std;

class Stack {
    vector<int> v;

    public:

    int get_size() {
        return v.size();
    }

    int top() {
        return v.back();
    }

    void push_back(int x) {
        v.push_back(x);
    }

    void pop_back() {
        v.pop_back();
    }
};

void print_stack(Stack& s) {
    if (s.get_size() == 0) {
        cout << "Empty stack" << endl;
        return;
    }

    cout << "Top element: " << s.top();

    cout << " (size: " << s.get_size() << ")";
    cout << endl;
}

int main() {
    cout << "Initializing stack:" << endl;
    Stack s;
    print_stack(s);
    cout << endl;

    cout << "Adding some elements" << endl;
    for (int i = 0; i < 5; i++)
        s.push_back(i);
    print_stack(s);
    cout << endl;

    cout << "Removing elements from stack" << endl;
    while (s.get_size() > 0) {
        s.pop_back();
        print_stack(s);
    }
    
    return 0;
}