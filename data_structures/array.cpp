#include <iostream>
#include <cassert>
#include <chrono>

using namespace std;
using namespace std::chrono;

class DynamicArray {
    private:
        int size;
        int* arr;

    public:
        DynamicArray(int size = 0, int default_value = 0) {
            this->size = size;
            this->arr = new int[size];
            for (int i = 0; i < size; i++)
                arr[i] = default_value;
        }

        ~DynamicArray() {
            delete[] arr;
        }

        int get_size() {
            return this->size;
        }

        int get(int i) {
            assert((i >= 0) && (i < size));
            return arr[i];
        }

        void set(int i, int x) {
            assert((i >= 0) && (i < size));
            arr[i] = x;
        }

        void push_back(int x) {
            int* arr2 = new int[this->size + 1];
            for (int i = 0; i < this->size; i++)
                arr2[i] = arr[i];

            this->size += 1;
            arr2[this->size-1] = x;

            delete[] arr;
            arr = arr2;
        }

        void pop_back() {
            int* arr2 = new int[this->size - 1];
            for (int i = 0; i < this->size-1; i++)
                arr2[i] = arr[i];

            this->size -= 1;

            delete[] arr;
            arr = arr2;
        }
};

void print_array(DynamicArray& v) {
    if (v.get_size() == 0) {
        cout << "Empty array" << endl;
    }

    for (int i = 0; i < v.get_size(); i++)
        cout << v.get(i) << " ";

    cout << " (size: " << v.get_size() << ")";
    cout << endl;
}

int main() {
    cout << "Initializing DynamicArray of size 5:" << endl;
    DynamicArray v(5, -1);
    print_array(v);

    cout << "Element at index 1:" << endl;
    cout << v.get(1) << endl;

    cout << "Assigning v[2] with value 10:" << endl;
    v.set(2, 10);
    print_array(v);

    cout << "push_back(100):" << endl;
    v.push_back(100);
    print_array(v);

    cout << "pop_back()" << endl;
    v.pop_back();
    print_array(v);

    cout << "Let's test push_back method" << endl;
    auto start = high_resolution_clock::now();

    DynamicArray v2(0);
    for (int i = 0; i < 100000; i++)
        v2.push_back(i);

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);
    cout << "Time taken: " << duration.count() << " milliseconds" << endl;

    return 0;
}