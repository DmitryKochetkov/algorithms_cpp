#include <iostream>
#include <cassert>
#include <chrono>

using namespace std;
using namespace std::chrono;

struct Node {
    int value;
    Node* next;
    Node* prev;

    Node(int value) {
        this->value = value;
        this->next = nullptr;
        this->prev = nullptr;
    }
};

class List {
    int size;
    Node* head;
    Node* tail;

    public:

    int get_size() {
        return this->size;
    }

    Node* find(int index) {
        assert((index >= 0) && (index < size));
        Node* result = head;

        for (int i = 0; i < index; i++)
            result = result->next;

        return result;
    }

    void push_back(int x) {
        if (head == nullptr) {
            head = new Node(x);
            tail = head;
        }
        else {
            Node* node = new Node(x);
            Node* old_tail = tail;

            tail = node;
            old_tail->next = tail;
            tail->prev = old_tail;
        }
        this->size += 1;
    }

    void push_front(int x) {
        if (head == nullptr) {
            head = new Node(x);
        }
        else {
            Node* node = new Node(x);
            node->next = head;
            head = node;
        }
        this->size += 1;
    }

    List(int size = 0, int default_value = 0) {
        this->size = 0;
        this->head = nullptr;
        this->tail = nullptr;
        
        for (int i = 0; i < size; i++)
            this->push_back(default_value);
    }

    ~List() {
        Node* current = head;
        while (current != nullptr) {
            Node* next = current->next;
            delete current;
            current = next;
        }
    } 

    int get(int index) {
        return find(index)->value;
    }

    void set(int i, int x) {
        assert((i >= 0) && (i < size));
        Node* node = find(i);
        node->value = x;
    }

    void pop_back() {
        if (size == 0)
            return;
        
        if (size == 1) {
            delete head;
            head = nullptr;
            tail = nullptr;
        }
        else {
            Node* temp = tail->prev;
            delete tail;
            tail = temp;
            tail->next = nullptr;
        }
        size -= 1;
    }
};

void print_list(List& v) {
    if (v.get_size() == 0) {
        cout << "Empty list" << endl;
        return;
    }

    for (int i = 0; i < v.get_size(); i++)
        cout << v.get(i) << " ";

    cout << " (size: " << v.get_size() << ")";
    cout << endl;
}

int main() {
    cout << "Initializing list of size 5:" << endl;
    List v(5, -1);
    print_list(v);
    cout << endl;

    cout << "Element at index 1:" << endl;
    cout << v.get(1) << endl;
    cout << endl;

    cout << "Assigning v[2] with value 10:" << endl;
    v.set(2, 10);
    print_list(v);
    cout << endl;

    cout << "push_back(100):" << endl;
    v.push_back(100);
    print_list(v);
    cout << endl;

    cout << "push_front(-100):" << endl;
    v.push_front(-100);
    print_list(v);
    cout << endl;

    cout << "pop_back()" << endl;
    v.pop_back();
    print_list(v);
    cout << endl;

    cout << "Let's pop all elements" << endl;

    while (v.get_size() > 0) {
        v.pop_back();
        print_list(v);
    }
    cout << endl;

    cout << "Let's test push_front method" << endl;
    auto start = high_resolution_clock::now();

    List v2(0);
    for (int i = 0; i < 100000; i++)
        v2.push_front(i);

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);
    cout << "Time taken: " << duration.count() << " milliseconds" << endl;

    cout << "Let's test push_back method" << endl;
    start = high_resolution_clock::now();

    List v3(0);
    for (int i = 0; i < 100000; i++)
        v3.push_back(i);

    stop = high_resolution_clock::now();
    duration = duration_cast<milliseconds>(stop - start);
    cout << "Time taken: " << duration.count() << " milliseconds" << endl;
    return 0;
}