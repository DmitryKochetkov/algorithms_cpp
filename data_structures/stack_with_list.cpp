#include <iostream>
#include <cassert>
#include <chrono>

using namespace std;
using namespace std::chrono;

struct Node {
    int value;
    Node* next;
    Node* prev;

    Node(int value) {
        this->value = value;
        this->next = nullptr;
        this->prev = nullptr;
    }
};

class List {
    int size;
    Node* head;
    Node* tail;

    public:

    int get_size() {
        return this->size;
    }

    Node* find(int index) {
        assert((index >= 0) && (index < size));
        Node* result = head;

        for (int i = 0; i < index; i++)
            result = result->next;

        return result;
    }

    void push_back(int x) {
        if (head == nullptr) {
            head = new Node(x);
            tail = head;
        }
        else {
            Node* node = new Node(x);
            Node* old_tail = tail;

            tail = node;
            old_tail->next = tail;
            tail->prev = old_tail;
        }
        this->size += 1;
    }

    void push_front(int x) {
        if (head == nullptr) {
            head = new Node(x);
        }
        else {
            Node* node = new Node(x);
            node->next = head;
            head = node;
        }
        this->size += 1;
    }

    List(int size = 0, int default_value = 0) {
        this->size = 0;
        this->head = nullptr;
        this->tail = nullptr;
        
        for (int i = 0; i < size; i++)
            this->push_back(default_value);
    }

    ~List() {
        Node* current = head;
        while (current != nullptr) {
            Node* next = current->next;
            delete current;
            current = next;
        }
    } 

    int get(int index) {
        return find(index)->value;
    }

    void set(int i, int x) {
        assert((i >= 0) && (i < size));
        Node* node = find(i);
        node->value = x;
    }

    void pop_back() {
        if (size == 0)
            return;
        
        if (size == 1) {
            delete head;
            head = nullptr;
            tail = nullptr;
        }
        else {
            Node* temp = tail->prev;
            delete tail;
            tail = temp;
            tail->next = nullptr;
        }
        this->size -= 1;
    }
};

class Stack {
    List list;

    public:

    int get_size() {
        return list.get_size();
    }

    int top() {
        return list.get(list.get_size() - 1);
    }

    void push_back(int x) {
        list.push_back(x);
    }

    void pop_back() {
        list.pop_back();
    }
};

void print_list(List& v) {
    if (v.get_size() == 0) {
        cout << "Empty list" << endl;
        return;
    }

    for (int i = 0; i < v.get_size(); i++)
        cout << v.get(i) << " ";

    cout << " (size: " << v.get_size() << ")";
    cout << endl;
}

void print_stack(Stack& s) {
    if (s.get_size() == 0) {
        cout << "Empty stack" << endl;
        return;
    }

    cout << "Top element: " << s.top();

    cout << " (size: " << s.get_size() << ")";
    cout << endl;
}

int main() {
    cout << "Initializing stack:" << endl;
    Stack s;
    print_stack(s);
    cout << endl;

    cout << "Adding some elements" << endl;
    for (int i = 0; i < 5; i++) {
        s.push_back(i);
        print_stack(s);
    }
    
    cout << endl;

    cout << "Removing elements from stack" << endl;
    while (s.get_size() > 0) {
        s.pop_back();
        print_stack(s);
    }
    return 0;
}